module.exports = {
  plugins: [
    // require('stylelint'),
    require('postcss-object-fit-images'),
    require('autoprefixer'),
    require('cssnano'),
  ]
}
